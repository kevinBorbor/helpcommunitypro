package com.example.semestreproyectohc.Packages.Disenos;

import androidx.appcompat.app.AppCompatActivity;
import com.androidnetworking.common.Priority;
import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.semestreproyectohc.R;

import org.json.JSONException;
import org.json.JSONObject;

public class Chat extends AppCompatActivity {
    TextView messagesTextView;
    EditText inputEditText;
    Button sendButton;
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        context = this;
        messagesTextView = findViewById(R.id.messagesTextView);
        inputEditText = findViewById(R.id.inputEditText);
        sendButton = findViewById(R.id.sendButton);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String input = inputEditText.getText().toString();
                messagesTextView.append(Html.fromHtml("<p><b>Tu:</b> " + input + "</p>"));
                inputEditText.setText("");
                getResponse(input);
            }
        });
    }
    private void getResponse(String input) {
        String workspaceId = "81a35c9e-35c5-4db9-adf6-07a369ac79e8";

        String urlAssistant= "https://api.us-south.assistant.watson.cloud.ibm.com/instances/e6a5e199-6a62-473b-91ad-46efa0764b42/v1/workspaces/"+workspaceId+"/message";
        String authentication = "29lILHeYGbxc7bfKdkFiAarAIDJ11zFU9WePK7zFWzrS";

        //creo la estructura json de input del usuario
        JSONObject inputJsonObject = new JSONObject();
        try {
            inputJsonObject.put("text",input);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("input", inputJsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AndroidNetworking.post(urlAssistant)
                .addHeaders("Content-Type","application/json")
                .addHeaders("Authorization","Basic " + authentication)
                .addJSONObjectBody(jsonBody)
                .setPriority(Priority.HIGH)
                .setTag(getString(R.string.app_name))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        String outputAssistant = "";


                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }
}