package com.example.semestreproyectohc.Packages.Disenos;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.semestreproyectohc.Packages.Model.Casos;
import com.example.semestreproyectohc.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

public class Vehiculo extends AppCompatActivity {
    ImageView imageView;
    String user;
    TextView us;
    EditText nombre,descripcion;
    Casos casos;
    private StorageReference myReferenceStorage;
    FirebaseAuth mAuth;
    FirebaseFirestore db;
    DatabaseReference mDbaseRe;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehivulos);
        casos=new Casos();
        mAuth=FirebaseAuth.getInstance();
        db=FirebaseFirestore.getInstance();
        imageView=(ImageView)findViewById(R.id.miObjeto);
        us=(TextView)findViewById(R.id.nombreObjetoU);
        nombre=(EditText)findViewById(R.id.nombreObjeto);
        descripcion=(EditText)findViewById(R.id.descripcionObjeto);
        myReferenceStorage= FirebaseStorage.getInstance().getReference();
        Bundle bundle=this.getIntent().getExtras();//
        if (bundle!=null){
            user=bundle.getString("e");
            us.setText(user);
        }else {
            Toast.makeText(this,"Vacio",Toast.LENGTH_LONG).show();
        }



    }
    public void subirI(View view) {
        cargarImagen();
    }
    private void cargarImagen() {
        Intent intent=new Intent(Intent.ACTION_PICK, MediaStore.Images
                .Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/");
        startActivityForResult(intent.createChooser(intent,"Seleccione una Aplicación"),10);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode==RESULT_OK){

            Uri path=data.getData();
            imageView.setImageURI(path);

            if(imageView.getDrawable()==null){
                Toast.makeText(getApplicationContext(),"Selecione Una Imagen",Toast.LENGTH_SHORT).show();

            }else{


                Toast.makeText(getApplicationContext(),"imageName",Toast.LENGTH_SHORT).show();
                Toast.makeText(getApplicationContext(),path.getLastPathSegment(),Toast.LENGTH_SHORT).show();
                final StorageReference filePath=myReferenceStorage.child("Imagenes").child(path.getLastPathSegment());
                filePath.putFile(path).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        Toast.makeText(getApplicationContext(),"Si subio",Toast.LENGTH_SHORT).show();
                        Task<Uri> descargar=filePath.getDownloadUrl();
                        String d= String.valueOf(descargar.getResult());

                        Toast.makeText(getApplicationContext(),"URL" + d,Toast.LENGTH_SHORT).show();
                        String n=nombre.getText().toString();
                        String de=descripcion.getText().toString();
                        if (n==null ||de==null  ){
                            Toast.makeText(getApplicationContext(),"Datos Vacios",Toast.LENGTH_SHORT).show();
                            casos.setTipo("Objetos");
                            casos.setNombre("No Hay descripcion");
                            casos.setDescripcion("No hay descripcion");
                            casos.setUbicacion(d);
                            casos.setUsuario(us.getText().toString());
                        }
                        else{
                            casos.setTipo("Objetos");
                            casos.setNombre(n);
                            casos.setDescripcion(d);
                            casos.setUbicacion(d);
                            casos.setUsuario(us.getText().toString());
                        }




                    }
                });
            }
        }
    }
}