package com.example.semestreproyectohc.Packages.Actividades;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.example.semestreproyectohc.Packages.Model.User;
import com.example.semestreproyectohc.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.UUID;

public class RegistroUsuario extends AppCompatActivity {

    Button registrarUsuario;
    EditText nombre,apellido,email,password;
    RadioButton radio1,radio2;

    //---------------------------------------
    FirebaseAuth mAuth;
    FirebaseFirestore db;
    DatabaseReference mDbaseRe;
    //---------------------------------------
    ProgressDialog pd;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_usuario);
        mAuth=FirebaseAuth.getInstance();
        db=FirebaseFirestore.getInstance();
        user=new User();
        initViews();
    }

    public void initViews(){
        mDbaseRe= FirebaseDatabase.getInstance().getReference();
        registrarUsuario=(Button)findViewById(R.id.btn_registrar_usuario);
        nombre=(EditText)findViewById(R.id.input_nombres);
        apellido=(EditText)findViewById(R.id.input_apellidos);
        email=(EditText)findViewById(R.id.input_email);
        password=(EditText)findViewById(R.id.input_password);
        radio1=(RadioButton)findViewById(R.id.radio_admin);
        radio2=(RadioButton)findViewById(R.id.radio_user);


    }

    public Boolean validarInputsNombre(){
        String validarNombre=nombre.getText().toString().trim();
        if (validarNombre.isEmpty()){
            nombre.setError("Este campo es requerido");
            return false;
        }else{
            nombre.setError(null);
            return true;
        }
    }//end validar nombre
    public Boolean validarInputsApellido(){
        String validar=apellido.getText().toString().trim();
        if (validar.isEmpty()){
            apellido.setError("Este campo es requerido");
            return false;
        }else{
            apellido.setError(null);
            return true;
        }
    }//end validar apellido
    public Boolean validarCorreo(){
        String validar=email.getText().toString().trim();
        if (validar.isEmpty()){
            email.setError("Este campo es requerido");
            return false;
        }else{
            email.setError(null);
            return true;
        }
    }//end validar correo
    public Boolean validarPassword(){
        String validar=password.getText().toString().trim();
        if (validar.isEmpty()){
            password.setError("Este campo es requerido");
            return false;
        }else{
            password.setError(null);
            return true;
        }
    }//end validar nombre

       public String seleccionado(){
           if (radio1.isChecked()==true)
           {

           String tp;
               tp= "Administrador" ;
               return tp;
               } else   {
               String tp2;
               tp2= "Usuario" ;
               return tp2;
           }





       }
    public void ingresarUsario(View view) {

        if (!validarInputsNombre() | !validarInputsApellido() |!validarCorreo() | !validarPassword() ){ return;
        }

        metodoRegistarUsuario();
}

    private void metodoRegistarUsuario() {
        user.setEmail(email.getText().toString().trim());
        user.setPassword(password.getText().toString().trim());
        user.setNombres(nombre.getText().toString().trim());
        user.setApellidos(apellido.getText().toString().trim());
        user.setTipo_usuario(seleccionado());

        mAuth.createUserWithEmailAndPassword(user.getEmail(),user.getPassword()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()){

                    String id=(mAuth.getCurrentUser().getUid());
                    db.collection("Usuarios").document(id).set(user).
                            addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {

                                    Toast.makeText(getApplicationContext(),"Cargando.....",Toast.LENGTH_SHORT).show();
                                    startActivity(new Intent(RegistroUsuario.this,Login.class));
                                }
                            }).
                            addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Toast.makeText(getApplicationContext(),"Cargando.....",Toast.LENGTH_SHORT).show();

                                    Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_SHORT).show();
                                }
                            });
                }else{
                    Toast.makeText(getApplicationContext(),"No se pudo registrar usuario",Toast.LENGTH_SHORT).show();

                }

            }
        });
    }


    public void tengoCuenta(View view) {
        startActivity(new Intent(RegistroUsuario.this,Login.class));
    }
}