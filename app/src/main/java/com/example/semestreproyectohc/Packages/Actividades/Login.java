package com.example.semestreproyectohc.Packages.Actividades;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.semestreproyectohc.Packages.Model.User;
import com.example.semestreproyectohc.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

public class Login extends AppCompatActivity {
 Button btnregistro,btnLogin;
 EditText email,password;
    FirebaseAuth mAuth;
    FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mAuth=FirebaseAuth.getInstance();
        db=FirebaseFirestore.getInstance();
        initViews();

    }
    public  void initViews(){
        mAuth=FirebaseAuth.getInstance();
        btnLogin=(Button)findViewById(R.id.btnIngresar);
        btnregistro=(Button)findViewById(R.id.btnRegistrar);
        email=(EditText)findViewById(R.id.editTextTextEmailAddress);
        password=(EditText)findViewById(R.id.input_user_login);
    }

    public void logearse(View view) {
        String user=(email.getText().toString().trim());
        String clave=(password.getText().toString().trim());
        if (!user.isEmpty() && !clave.isEmpty()){
            mAuth.signInWithEmailAndPassword(user,clave).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {

                    startActivity(new Intent(Login.this,Menu.class));
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(getApplicationContext(),"NO ingreso ",Toast.LENGTH_SHORT).show();
                }
            });
        }else {
            Toast.makeText(getApplicationContext(),"Campos vacios",Toast.LENGTH_SHORT).show();
        }
    }

    public void volverRegistro(View view) {
        startActivity(new Intent(Login.this,RegistroUsuario.class));
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(mAuth.getCurrentUser()!=null){
            startActivity(new Intent(Login.this,Menu.class));

        }
    }
}